﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ReflectionTestAssembly
{
    public interface Interface1
    {
        void Method2(IEnumerable<Lazy<Task<double>>> x);
        event EventHandler<EventArgs> Event3;
        int Property1 { get; }
    }
}
