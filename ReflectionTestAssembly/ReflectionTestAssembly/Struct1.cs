﻿namespace ReflectionTestAssembly
{
    public struct Struct1
    {
        private int a;

        public Struct1(int x)
        {
            a = x;
        }

        public int A => a;

        public static Struct1 operator +(Struct1 a, Struct1 b)
        {
            return new Struct1(a.A + b.A);
        }
    }
}