﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using Ninject;

namespace TextParser
{
    public class ParserManager
    {
        private const string ParsersLocation = @"Parsers\*.dll";

        public ParserManager()
        {
            Thread.Sleep(5000);
            var kernel = new StandardKernel();

            var location = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) ?? string.Empty;

            kernel.Load(Path.Combine(location, ParsersLocation));

            Parsers = new ReadOnlyCollection<IParser>(kernel.GetAll<IParser>().ToList());
        }

        public IReadOnlyCollection<IParser> GetParsers()
        {
            return Parsers.OrderBy(p => p.ToString()).ToList();
        }
        public IReadOnlyCollection<IParser> Parsers { get; }
    }
}
