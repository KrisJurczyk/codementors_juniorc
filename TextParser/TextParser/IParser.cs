﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Common.Annotations;

namespace TextParser
{
    public interface IParser
    {
        Task ParseFileAsync([NotNull] string inputFile, [NotNull] string outputFile, CancellationToken ct);

        [Pure, NotNull]
        Task<string> ParseTextAsync([NotNull]string input, CancellationToken ct);

    }
}
