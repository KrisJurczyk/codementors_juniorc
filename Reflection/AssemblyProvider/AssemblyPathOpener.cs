﻿using System.Reflection;
using Interfaces;
using JetBrains.Annotations;

namespace AssemblyProvider
{
    public class AssemblyPathOpener : IAssemblyOpener
    {
        public Assembly LoadedAssembly { get; }

        public AssemblyPathOpener([NotNull]IPathProvider pathToAssembly)
        {
            LoadedAssembly = Assembly.LoadFile(pathToAssembly.PathFile);
        }
    }
}
