﻿using System;
using Interfaces;

namespace PathProvider
{
    public class PathArgProvider : IPathProvider
    {
        public string PathFile { get; } = Environment.GetCommandLineArgs()[1];

    }
}
