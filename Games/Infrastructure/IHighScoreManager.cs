﻿namespace Infrastructure
{
    public interface IHighScoreManager
    {
        void Add(HighscoreEntry entry);
        HighscoreEntry[] List();
    }
}